# LAMP-dockerfile

## Dockerfile con lamp stack + phpmyadmin.

Para la correcta ejecucion de este dockerfile va a necesitar primero seguir una serie de pasos.

1. El primero es modificar su archivo hosts para que apunte al servername que vayamos a utilizar a la IP de la maquina donde va a correr el container.

2. Una vez ya actualizado el archivo hosts, vamos a correr un container de [nginx proxy](https://hub.docker.com/r/jwilder/nginx-proxy) con el comando `docker run -d -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy`

3. Ya con nuestro archivo hosts modificado y nuestro nginx reverse proxy estamos listos para poder seguir con nuestro dockerfile, es importante que hayamos configurado nuestro archivo default.conf de acuerdo al servername que hayamos elegido y usado en el archivo hosts. 

4. Primero tendremos que construir nuestra imagen con el comando docker build -t <nombre de la imagen>:v<version de la imagen> <path donde esta el dockerfile>, por ejemplo `docker build -t lamp-pma:v1 .` 

5. Una vez hecho el build, podremos correr nuestro dockerfile con el comando `docker run -d --name actualizacion.tecnologica -e PMA_USER=zdenko -e PMA_PASS=040998 -e PHP_VER=5.1.1 -e VIRTUAL_HOST=actualizacion.tecnologica -v $(pwd)/files/phpmyadmin:/var/www/html/phpmyadmin/ lamp-pma:v1` entre los distintos parametros, destacaremos las variables de entorno, siendo la primera el nombre del usuario de admin que usaremos para el phpmyadmin y el segundo su contraseña, la tercera seria la version del phpmyadmin a instalar (el cual si nuestro directorio phpmyadmin dentro del directorio files esta vacio, se instalara y populara una vez iniciado el container) y por ultimo pero no menos importante el nombre del virtual host, esto nos permitira acceder directamente a traves del nombre actualizacion.tecnologica/phpmyadmin una vez inicializados ambos contenedores. Por ultimo como mencionamos antes el volumen que nos permitira ahorrarnos el paso de instalar el phpmyadmin si es que ya tenemos una version o si ya tenemos una configuracion que queremos copiar. 


